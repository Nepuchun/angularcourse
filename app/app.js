'use strict';

var app = angular.module('app', ['ngRoute']);

app.constant('API_URL', 'http://api-album/');

app.constant('CLIENT_INFO', {
	grant_type: 'password',
	client_id: 'webapp',
	client_secret: 'webapp'
});

app.run(function($rootScope, API_URL){
	$rootScope.API_URL = API_URL;
})


