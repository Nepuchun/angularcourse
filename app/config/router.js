'use strict';

app.config(function($routeProvider){

	$routeProvider
		.when('/', {
			templateUrl: 'app/views/partials/home.html',
			controller: 'HomeCtrl'
		})
		.when('/albums', {
			templateUrl: 'app/views/partials/albums.html',
			controller: 'AlbumsCtrl'
		})
		.when('/album/:id', {
			templateUrl: 'app/views/partials/album.html',
			controller: 'AlbumCtrl'
		})
		.otherwise({
			redirectTo: '/'
		});

});
