'use strict';

app.controller('AlbumsCtrl', function($scope, AlbumFullService, $rootScope){

	$scope.albums = AlbumFullService.getAll().success(function(data){
		$scope.albums = data;
		console.log(data);
	});

	$scope.urlimg = $rootScope.API_URL;

	console.log('AlbumsCtrl');
});
