'use strict';

app.controller('MenuCtrl', function($scope, $location){
    $scope.templateMenu = 'app/views/partials/menu.html';

    $scope.isActive = function(path){
        return $location.path() === path;
    }
});
