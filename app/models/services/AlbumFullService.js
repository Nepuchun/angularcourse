'use strict';

app.service('AlbumFullService', function($http, API_URL){


	/**
	 * get album by id
	 * @param  {int} id
	 * @return HttpPromise
	 */
	function get(id){

		// url = API_URL + '/albumfull/' + id;

		// http.get(url).then(httpSuccess, httpError);


		return $http({
			method: 'GET',
			url: API_URL + 'albumfull/' + id,
			cache: false
		})
	}



	/**
	 * getAll album
	 * @param  Object JSON data
	 * Object Key : author : id author
	 * 				genre  : id genre
	 * 				user   : id user
	 *     			type   : sell | search | exchange
	 *     			last   : sort by date
	 *     			size   : number of element by page
	 *
	 *
	 * @return HttpPromise
	 */
	function getAll(data){
		return $http({
			method: 'GET',
			url: API_URL + 'albumfull',
			params: data,
			cache: false
		})
	}


	return ({
		get : get,
		getAll : getAll
	});

});
