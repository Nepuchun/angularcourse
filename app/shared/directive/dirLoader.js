'use strict';

app.directive('dirLoader', function(){

	return {
		restrict : 'A',
		scope: false, 
		link: function(scope, element, attr){
			console.log(scope);
			console.log(element);
			console.log(attr);

			scope.$watch(attr.dirLoader, function (value){
				if(angular.isDefine(value.finally) && typeof value.finally === 'function'){
					element.addClass = 'loader';
					console.log('addClass');
					value.error(function(data){
						// element.removeClass = 'loader';
						console.log('removeClass');
						// element.html('<strong></strong>');
					});

					value.finally(function(){
						element.removeClass = 'loader';
					})
				};
			});
		}
	}
});