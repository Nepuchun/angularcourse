'use strict';

app.filter('truncate', function(){
	return function(input, length){
		if(angular.isString(input)){
			return input.length > length ? input.substring(0, length) + '...' : input;
		}
		return input;
	}
});
